from flask import Flask, request
import json, redis, datetime, uuid, sqlite3
import numpy as np
from hotqueue import HotQueue
from sqlite3 import Error
import os.path

q = HotQueue("queue",host="redis",port=6379,db=1)

# relative path to the databases
jobsDATABASE = r'../data/jobs.db'
jobsMY_PATH = os.path.abspath(os.path.dirname(__file__))
jobsDATABASE = os.path.join(jobsMY_PATH, jobsDATABASE)

def create_jobs_connection():
	conn = None
	try: 
		conn = sqlite3.connect(jobsDATABASE)
	except Error as e:
		print(e)
	return conn

def create_jobs_db():
	jobs = create_jobs_connection()
	c = jobs.cursor()
	c.execute("""CREATE TABLE jobs (
		jobID text,
		status text,
		cdate text,
		command text,
		parameter text
		)""")

	c.execute("""CREATE TABLE addition (
		jobID text,
		status text,
		cdate text,
		command text,
		parameter text
		)""")
	
	c.execute("""CREATE TABLE outputs (
		jobID text,
		command text,
		paramter text,
		output text
		)""")

	jobs.close()
	return 'Done'

#create_jobs_db()

def new_job(command,parameter):
	jobID = str(uuid.uuid4())
	status = 'pending'
	cdate = str(datetime.datetime.now())
	command = command
	parameter = parameter

	jobs_conn = create_jobs_connection()
	jobs_c = jobs_conn.cursor()
	job=(jobID,status,cdate,command,parameter)
	jobs_c.execute("INSERT INTO jobs VALUES (?,?,?,?,?)",(job))
	jobs_conn.commit()
	q.put(jobID)
	print('Your Job ID is {}'.format(jobID))
	return jobID

def new_add_job(head,arms,legs,tails):
	jobID = str(uuid.uuid4())
	status = 'pending'
	cdate = str(datetime.datetime.now())
	command = 'add'
	addID = str(uuid.uuid4())
	animalID = str(uuid.uuid4())
	parameter = addID
	
	jobs_conn = create_jobs_connection()
	jobs_c = jobs_conn.cursor()
	job=(jobID,status,cdate,command,parameter)
	edit=(addID,animalID,head,arms,legs,tails,cdate)
	jobs_c.execute("INSERT INTO jobs VALUES (?,?,?,?,?)",(job))
	jobs_conn.commit()
	jobs_c.execute("INSERT INTO edits VALUES (?,?,?,?,?,?,?)",(edit))
	jobs_conn.commit()
	q.put(jobID)
	print('Your Job ID is {}'.format(jobID))
	return [jobID,addID]
	
def get_jobs():
	jobs_conn = create_jobs_connection()
	jobs_c = jobs_conn.cursor()
	jobs_c.execute("SELECT * FROM jobs")
	print(jobs_c.fetchall())

def get_jobs_by_id(jobID):
	jobs_conn = create_jobs_connection()
	jobs_c = jobs_conn.cursor()
	jobs_c.execute("SELECT * FROM jobs WHERE jobID = ?",(jobID,))
	job=jobs_c.fetchone()
	print(job)
	return job

def update_job(jobID):
	jobs_conn = create_jobs_connection()
	jobs_c = jobs_conn.cursor()
	job = get_jobs_by_id(jobID)
	if job[1] == 'pending':
		jobs_c.execute("""UPDATE jobs SET status = 'in progress'
				WHERE jobID = ?""",
				(jobID,))
		print('job {} in progress'.format(job[0]))
	elif job[1] == 'in progress':
		jobs_c.execute("""UPDATE jobs SET status = 'complete'
				WHERE jobID = ?""",
				(jobID,))
		print('job {} complete'.format(job[0]))
	jobs_conn.commit()



def getbyseason(parameter):
	isl_conn = create_animals_connection()
	isl_c = isl_conn.cursor()
	sy = int(parameter[4]+parameter[5]+parameter[6]+parameter[7])
	sm = int(parameter[0]+parameter[1])
	sd = int(parameter[2]+parameter[3])
	ey = int(parameter[12]+parameter[13]+parameter[14]+parameter[15])
	em = int(parameter[8]+parameter[9])
	ed = int(parameter[10]+parameter[11])
	start_dt = datetime.date(sy, sm, sd)
	end_dt = datetime.date(ey, em, ed)
	fulloutput = []
	for dt in daterange(start_dt, end_dt):
		print('Animals created on {}:'.format(dt))
		isl_c.execute("SELECT * FROM animals WHERE cdate = ?",(str(dt),))
		#print(type(dt.strftime("%Y-%m-%d")))
		output = isl_c.fetchall()
		#if not output:
		#	print('None'.format(dt))
		#	return ('No animals created in that date range\n')
		#else:
		#	for animal in output:
		#		print(animal)
		#		fulloutput.append(animal)
		for animal in output:
			print(animal)
			fulloutput.append(animal)
		print('\n')
	return fulloutput

def getbytime(parameter):
	with open('../data/messishots20182019.json') as f:
		data = json.load(f)
	output = []
	for shot in data:
		if shot['minute'] > int(parameter[0:2]) and shot['minute'] < int(parameter[2:4]):
			output.append(shot)
	return(output)	


def getall():
	with open('../data/messishots20182019.json') as f:
		data = json.load(f)
	print(data)
	return(data)
		
def meanlocation():
	with open('../data/messishots20182019.json') as f:
		data = json.load(f)
	x = []
	y = []
	for shot in data:
		x.append(shot['location'][0])
		y.append(shot['location'][1])
	
	output = [round(sum(x)/len(x),2),round(sum(y)/len(y),2)]
	print(output)
	return output

def meantime():
	with open('../data/messishots20182019.json') as f:
		data = json.load(f)
	minute = []
	second = []
	for shot in data:
		minute.append(shot['minute'])
		second.append(shot['second'])
	avgmin = sum(minute)/len(minute)
	avgsec = round((avgmin % 1 *60) + sum(second)/len(second))
	avgmin = int(avgmin)
	if avgsec > 60:
		avgsec = avgsec % 60
		avgmin += 1

	avg = '{}:{}'.format(avgmin,avgsec)
	print(avg)
	return avg

def meanxG():
	with open('../data/messishots20182019.json') as f:
		data = json.load(f)
	xG = []
	for shot in data:
		xG.append(shot['shot']['statsbomb_xg'])
	avg = round(sum(xG)/len(xG),2)
	print(avg)
	return avg 	
	
def addshot(new_shot): 
	with open('../data/messishots20182019.json', 'r+') as f:
		data = json.load(f)
	data.append(new_shot)
	json_file.seek(0)
	json.dump(data,json_file)
	return new_shot['id']

def add_output(job,output):
	conn = create_jobs_connection()
	c = conn.cursor()
	output = json.dumps(output)
	c.execute("INSERT INTO outputs VALUES (?,?,?,?)",(job[0],job[3],job[4],output))
	conn.commit()
	return output

def complete_task(command,parameter):
	if command == 'getall':
		return(getall())
	elif command == 'getbytime':
		return(getbytime(parameter))
	elif command == 'mean' and parameter == 'location':
		return(meanlocation())	
	elif command == 'mean' and parameter == 'time':
		return(meantime())
	elif command == 'mean' and parameter == 'xG':
		return meanxG()
	elif command == 'addshot':
		return addshot(parameter)
