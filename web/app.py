from flask import Flask, request
import redis, datetime, json, uuid, sqlite3
import jobs
from hotqueue import HotQueue
import os.path

app = Flask(__name__)

DATABASE = r'../data/jobs.db'
MY_PATH = os.path.abspath(os.path.dirname(__file__))
DATABASE = os.path.join(MY_PATH, DATABASE)

def create_connection():
	conn = None
	try:
		conn = sqlite3.connect(DATABASE)
	except Error as e:
		print(e)
	return conn

@app.route('/',methods=['GET'])
def general():
	jobID = jobs.new_job('getall','all')
	return 'Your JobID is {}\nto view job status: curl localhost:5009/status/{}\nto view job output: curl localhost:5009/output/{}\n'.format(jobID,jobID,jobID)

@app.route('/timerange/<firstminute>/<secondminute>', methods=['GET'])
def getbytime(firstminute,secondminute):
	if int(firstminute) % 10 == int(firstminute):
		firstminute = '0{}'.format(firstminute)
	if int(secondminute) % 10 == int(secondminute):
		secondminute = '0{}'.format(secondminute)
	jobID = jobs.new_job('getbytime', '{}{}'.format(firstminute,secondminute))
	return 'Your JobID is {}\nto view job status: curl localhost:5009/status/{}\nto view job output: curl localhost:5009/output/{}\n'.format(jobID,jobID,jobID)

@app.route('/mean/<parameter>', methods=['GET'])
def mean(parameter):
	jobID = jobs.new_job('mean','{}'.format(parameter))
	return 'Your JobID is {}\nto view job status: curl localhost:5009/status/{}\nto view job output: curl localhost:5009/output/{}\n'.format(jobID,jobID,jobID)

#@app.route('/outcome/<goal>', methods=['GET'])
#def outcome(goal):
	#jobID = jobs.new_job('outcome','{}'.format(parameter))
	#return 'Your JobID is {}\nto view job status: curl localhost:5009/status/{}\nto view job output: curl localhost:5009/output/{}\n'.format(jobID,jobID,jobID)

@app.route('/addshot', methods=['POST'])
def addshot():
	new_shot = {
		'id': str(uuid.uuid4()),
		'index': request.form['index'],
		'period': request.form['period'],
		'timestamp': request.form['timestamp'],
		'minute': request.form['minute'],
		'second': request.form['second'],
		'type': request.form['type'],
		'possession': request.form['possession'],
		'possession_team': request.form['possession_team'],
		'play_pattern': request.form['play_pattern'],
		'team': request.form['team'],
		'player': request.form['player'],
		'position': request.form['position'],
		'location': request.form['location'],
		'duration': request.form['duration'],
		'related_events': request.form['related_events'],
		'shot': request.form['shot'],
		'match_id': request.form['match_id'],
		'under_pressure': request.form['under_pressure'],
		'out': request.form['out']
	}
	jobs.new_job('addshot',new_shot)
	return 'Your JobID is {}\nto view job status: curl localhost:5009/status/{}\nto view job output: curl localhost:5009/output/{}\n'.format(jobID,jobID,jobID)
	

@app.route('/output/<jobID>', methods = ['GET'])
def getoutput(jobID):
	conn = create_connection()
	c = conn.cursor()
	c.execute("SELECT * FROM outputs WHERE jobID = ?", (jobID,))
	output = c.fetchone()
	if output[1] == 'getall' or output[1] == 'getbytime':
		print(output[3])
		output = json.loads(output[3])	
		stroutput = ''
		for shot in output:
			stroutput += 'half: {}, timestamp: {}, location: {}, xG: {}, outcome: {}, type of shot: {}, body part: {}\n'.format(shot['period'],shot['timestamp'], shot['location'], shot['shot']['statsbomb_xg'], shot['shot']['outcome']['name'], shot['shot']['type']['name'], shot['shot']['body_part']['name'])
		return stroutput
	elif output[1] == 'mean' and output[2] == 'location':
		print(output[3])
		return 'Mean Shot Location: {}\n'.format(output[3])
	elif output[1] == 'mean' and output[2] == 'time':
		stroutput = output[3][2:7]
		print(output[3][2:7])
		return 'Mean Shot Time: {}\n'.format(output[3][1:6])
	elif output[1] == 'mean' and output[2] == 'xG':
		return 'Mean xG Value: {}\n'.format(output[3])
	elif output[1] == 'addshot':
		return 'New Shot ID: {}'.format(output[3])

@app.route('/status/<jobID>', methods=['GET'])
def status(jobID):
	conn = create_connection()
	c = conn.cursor()
	c.execute("SELECT * FROM jobs WHERE jobID = ?",(jobID,))
	output = c.fetchone()
	return 'Job Status: {}\n'.format(output[1])
	
if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
