import datetime, sqlite3
from hotqueue import HotQueue
import jobs

q = HotQueue("queue",host="redis",port=6379,db=1)


@q.worker
def do_work(job):
	
	job = jobs.get_jobs_by_id(job)
	jobID = job[0]
	jobs.update_job(jobID)
	output = jobs.complete_task(job[3],job[4])
	jobs.update_job(jobID)
	jobs.add_output(job,output)

do_work()

#check length of queue and add more workers if queue too long
#if len(q) >= 10:
#	do_work()
#elif len(q) >= 20:
#	do_work()
#	do_work()

