John Paul Borsos jpb3649
COE332 Final Project

For my final project, I chose to use some of StatsBomb's, a soccer data collection, analytics and consulting company, free data about Messi's career. I specifically focused on his shot data from the 2018/2019 season and used the statsbombpy API to collect this data. Each data point is a shot taken by Messi that season, and is timestamped by the time in the game in which it took place. 

Tools used:
- sqlite3 database to store the information on the jobs and their outputs
- json file to store all of the shot data
- redis hotqueue to keep track of all the jobs and feed them to the worker
- flask to create my api

Routes I created:
- One that lists all of the shots and their specific KPIs I wanted to focus on, such as shot location, time within the game, the outcome, the type of shot, the body part, and the expected goals (xG) value (which demonstrates the value of a shot by conveying the probability it results in a goal, based on the shot's location, opposing players' location, how the shot was assisted, and what body part it was taken with). 
- One which allows the user to index the database by time in order to find the shots taken within those minutes in the games. 
- One which allows the user to find the means of three specific KPIs over the course of that season. These are the average shot location, the average time in the game, and the average xG value.
- One which allows the user to upload a new data point in the same json dictionary format as the statsbombpy API.
- One which allows the user to see the status of their job.
- One which allows the user to see the output of their job.

SPEC:
- Get All: 
route: '/'; method: GET; parameters: none
- Get Those in Time Range:
route: '/timerange/<firstminute>/<secondminute>'; method: GET; parameters: the start of the time range and the end of the time range (eg. '04' or '4')
- Find Mean:
route: '/mean/<parameter>'; method: GET; parameter: which KPI you'd like the mean value of (location, time, xG)
- Add Shot:
route: '/addshot'; method: POST; parameter: json dictionary in same format as statsbombpy data
- Get Status:
route: '/status/<jobID>'; method: GET; parameter: jobID (in uuid4 format)
- Get Output:
route: '/status/<jobID>'; method: GET; parameter: jobID (in uuid4 format)

Example JSON Output of a Single Shot:
[{"id":"8c95e369-ec54-4a84-9d3e-51abb6f38e6e","index":835,"period":1,"timestamp":"00:17:13.830","minute":17,"second":13,"type":"Shot","possession":37,"possession_team":"Barcelona","play_pattern":"From Free Kick","team":"Barcelona","player":"Lionel Andr\u00e9s Messi Cuccittini","position":"Left Center Forward","location":[95.6,44.4],"duration":1.046942,"related_events":["0b60e19e-2902-49c4-aad0-05624d981cfd"],"shot":{"statsbomb_xg":0.12733692,"end_location":[120.0,36.8,2.2],"type":{"id":62,"name":"Free Kick"},"body_part":{"id":38,"name":"Left Foot"},"technique":{"id":93,"name":"Normal"},"outcome":{"id":97,"name":"Goal"},"freeze_frame":[{"location":[107.4,43.7],"player":{"id":6648,"name":"A\u00efssa Mandi"},"position":{"id":3,"name":"Right Center Back"},"teammate":false},{"location":[108.4,42.2],"player":{"id":5563,"name":"Jos\u00e9 Andr\u00e9s Guardado Hern\u00e1ndez"},"position":{"id":15,"name":"Left Center Midfield"},"teammate":false},{"location":[106.5,44.4],"player":{"id":6651,"name":"Joaqu\u00edn S\u00e1nchez Rodr\u00edguez"},"position":{"id":7,"name":"Right Wing Back"},"teammate":false},{"location":[106.5,43.4],"player":{"id":7068,"name":"Marc Bartra Aregall"},"position":{"id":4,"name":"Center Back"},"teammate":false},{"location":[106.5,42.9],"player":{"id":6673,"name":"Sergio Canales Madrazo"},"position":{"id":13,"name":"Right Center Midfield"},"teammate":false},{"location":[106.4,45.5],"player":{"id":6658,"name":"Cristian Tello Herrera"},"position":{"id":8,"name":"Left Wing Back"},"teammate":false},{"location":[106.4,44.0],"player":{"id":5214,"name":"William Silva de Carvalho"},"position":{"id":10,"name":"Center Defensive Midfield"},"teammate":false},{"location":[106.2,39.0],"player":{"id":3313,"name":"Giovani Lo Celso"},"position":{"id":22,"name":"Right Center Forward"},"teammate":false},{"location":[107.5,42.7],"player":{"id":5203,"name":"Sergio Busquets i Burgos"},"position":{"id":13,"name":"Right Center Midfield"},"teammate":true},{"location":[107.5,41.9],"player":{"id":5470,"name":"Ivan Rakiti\u0107"},"position":{"id":12,"name":"Right Midfield"},"teammate":true},{"location":[106.9,49.7],"player":{"id":8206,"name":"Arturo Erasmo Vidal Pardo"},"position":{"id":16,"name":"Left Midfield"},"teammate":true},{"location":[107.6,40.6],"player":{"id":5213,"name":"Gerard Piqu\u00e9 Bernab\u00e9u"},"position":{"id":3,"name":"Right Center Back"},"teammate":true},{"location":[107.1,38.0],"player":{"id":11392,"name":"Arthur Henrique Ramos de Oliveira Melo"},"position":{"id":15,"name":"Left Center Midfield"},"teammate":true},{"location":[119.5,39.0],"player":{"id":6792,"name":"Pau L\u00f3pez Sabata"},"position":{"id":1,"name":"Goalkeeper"},"teammate":false},{"location":[106.7,34.7],"player":{"id":6777,"name":"Sidnei Rechel da Silva J\u00fanior"},"position":{"id":5,"name":"Left Center Back"},"teammate":false},{"location":[106.8,32.6],"player":{"id":6826,"name":"Cl\u00e9ment Lenglet"},"position":{"id":5,"name":"Left Center Back"},"teammate":true},{"location":[93.9,42.1],"player":{"id":5246,"name":"Luis Alberto Su\u00e1rez D\u00edaz"},"position":{"id":22,"name":"Right Center Forward"}]
